EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:standlight-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 1300 750  0    60   Input ~ 0
6VAC
Text GLabel 1300 850  0    60   Input ~ 0
6VAC2
$Comp
L Conn_01x02_Female 6VAC1
U 1 1 5A63712C
P 1500 750
F 0 "6VAC1" H 1500 850 50  0000 C CNN
F 1 "Conn_01x02_Female" H 1500 550 50  0000 C CNN
F 2 "TerminalBlocks_Phoenix:TerminalBlock_Phoenix_MKDS1.5-2pol" H 1500 750 50  0001 C CNN
F 3 "" H 1500 750 50  0001 C CNN
	1    1500 750 
	1    0    0    -1  
$EndComp
Text GLabel 1800 1450 2    60   Input ~ 0
6VAC2
Text GLabel 1200 1450 0    60   Input ~ 0
6VAC
$Comp
L D_Bridge_+-AA D1
U 1 1 5A637230
P 1500 1450
F 0 "D1" V 1500 1400 50  0000 L CNN
F 1 "D_Bridge_+-AA" V 1300 1600 50  0000 L CNN
F 2 "Diodes_THT:Diode_Bridge_Round_D9.8mm" H 1500 1450 50  0001 C CNN
F 3 "" H 1500 1450 50  0001 C CNN
	1    1500 1450
	0    -1   -1   0   
$EndComp
Text GLabel 3600 1150 2    60   Input ~ 0
+6V
Text GLabel 3600 1750 2    60   Input ~ 0
GND
Text GLabel 1250 2250 0    60   Input ~ 0
+6V
Text GLabel 1250 2350 0    60   Input ~ 0
GND
Text GLabel 1250 3050 0    60   Input ~ 0
+5V
Text GLabel 1250 3350 0    60   Input ~ 0
GND
Text GLabel 1300 4550 0    60   Input ~ 0
+5V
Text GLabel 1300 4650 0    60   Input ~ 0
GND
$Comp
L Conn_01x02_Female 5VFlashOut1
U 1 1 5A63811F
P 1800 5200
F 0 "5VFlashOut1" H 1800 5300 50  0000 C CNN
F 1 "Conn_01x02_Female" H 1800 5000 50  0000 C CNN
F 2 "TerminalBlocks_Phoenix:TerminalBlock_Phoenix_MKDS1.5-2pol" H 1800 5200 50  0001 C CNN
F 3 "" H 1800 5200 50  0001 C CNN
	1    1800 5200
	1    0    0    -1  
$EndComp
Text GLabel 1600 5300 0    60   Input ~ 0
GND
Text GLabel 1600 5200 0    60   Input ~ 0
+5V(StandFlash)
Text GLabel 6400 1900 2    60   Input ~ 0
+5VUP
$Comp
L R R1
U 1 1 5A638CD8
P 6250 1900
F 0 "R1" V 6330 1900 50  0000 C CNN
F 1 "1K" V 6250 1900 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 6180 1900 50  0001 C CNN
F 3 "" H 6250 1900 50  0001 C CNN
	1    6250 1900
	0    -1   -1   0   
$EndComp
$Comp
L LM555 U1
U 1 1 5A638CD9
P 5350 1900
F 0 "U1" H 4950 2250 50  0000 L CNN
F 1 "LM555" H 5450 2250 50  0000 L CNN
F 2 "Housings_DIP:DIP-8_W7.62mm_Socket" H 5350 1900 50  0001 C CNN
F 3 "" H 5350 1900 50  0001 C CNN
	1    5350 1900
	1    0    0    -1  
$EndComp
$Comp
L R R2
U 1 1 5A638CDA
P 6100 2050
F 0 "R2" V 6180 2050 50  0000 C CNN
F 1 "100K" V 6100 2050 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 6030 2050 50  0001 C CNN
F 3 "" H 6100 2050 50  0001 C CNN
	1    6100 2050
	1    0    0    -1  
$EndComp
Text GLabel 5200 2650 0    60   Input ~ 0
GND
Text GLabel 5850 1700 2    60   Input ~ 0
+5V(StandFlash)
Text GLabel 1250 2450 0    60   Input ~ 0
+5V
$Comp
L Conn_01x02_Female 5VOut1
U 1 1 5A638084
P 1500 4550
F 0 "5VOut1" H 1500 4650 50  0000 C CNN
F 1 "Conn_01x02_Female" H 1500 4350 50  0000 C CNN
F 2 "TerminalBlocks_Phoenix:TerminalBlock_Phoenix_MKDS1.5-2pol" H 1500 4550 50  0001 C CNN
F 3 "" H 1500 4550 50  0001 C CNN
	1    1500 4550
	1    0    0    -1  
$EndComp
Text GLabel 1550 3700 0    60   Input ~ 0
+5V(SuperCap)
Text GLabel 1550 3800 0    60   Input ~ 0
GND
Text GLabel 1550 3900 0    60   Input ~ 0
+5VUP
Text GLabel 5350 1500 1    60   Input ~ 0
+5VUP
Text GLabel 4850 2100 0    60   Input ~ 0
+5VUP
$Comp
L D D2
U 1 1 5A6A47A9
P 1400 3050
F 0 "D2" H 1400 3150 50  0000 C CNN
F 1 "D" H 1400 2950 50  0000 C CNN
F 2 "Diodes_THT:D_T-1_P5.08mm_Horizontal" H 1400 3050 50  0001 C CNN
F 3 "" H 1400 3050 50  0001 C CNN
	1    1400 3050
	-1   0    0    1   
$EndComp
$Comp
L Conn_01x03_Female PololuStepUp1
U 1 1 5A6BB269
P 1750 3800
F 0 "PololuStepUp1" H 1750 4000 50  0000 C CNN
F 1 "Conn_01x03_Female" H 1750 3600 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x03_Pitch2.54mm" H 1750 3800 50  0001 C CNN
F 3 "" H 1750 3800 50  0001 C CNN
	1    1750 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	5850 2200 5850 2100
Wire Wire Line
	4400 2350 4400 1700
Wire Wire Line
	4400 1700 4850 1700
Wire Wire Line
	5350 2650 5350 2300
Connection ~ 5200 2350
Wire Wire Line
	5200 2650 5350 2650
Wire Wire Line
	4400 2350 6100 2350
Wire Wire Line
	6100 2350 6100 2200
Wire Wire Line
	6100 2200 5850 2200
Wire Wire Line
	6100 1900 5850 1900
Wire Wire Line
	1250 3050 1250 3050
Wire Wire Line
	1550 3050 2150 3050
Wire Wire Line
	1250 3350 1950 3350
Wire Wire Line
	1500 1150 3600 1150
Wire Wire Line
	1500 1750 3600 1750
Wire Wire Line
	2750 1300 2750 1150
Connection ~ 2750 1150
Wire Wire Line
	2750 1600 2750 1750
Connection ~ 2750 1750
Wire Wire Line
	3500 1600 3500 1750
Connection ~ 3500 1750
Wire Wire Line
	3500 1300 3500 1150
Connection ~ 3500 1150
Connection ~ 1950 3050
Text GLabel 2150 3050 2    60   Input ~ 0
+5V(SuperCap)
$Comp
L Conn_01x05_Female PololuStepDown1
U 1 1 5A982B2D
P 1450 2250
F 0 "PololuStepDown1" H 1450 2550 50  0000 C CNN
F 1 "Conn_01x05_Female" H 1450 1950 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x05_Pitch2.54mm" H 1450 2250 50  0001 C CNN
F 3 "" H 1450 2250 50  0001 C CNN
	1    1450 2250
	1    0    0    -1  
$EndComp
$Comp
L CP1 C2
U 1 1 5ADF2F64
P 2750 1450
F 0 "C2" H 2775 1550 50  0000 L CNN
F 1 "25V 2200uf" H 2775 1350 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D13.0mm_P5.00mm" H 2750 1450 50  0001 C CNN
F 3 "" H 2750 1450 50  0001 C CNN
	1    2750 1450
	1    0    0    -1  
$EndComp
$Comp
L CP1 C3
U 1 1 5ADF3113
P 3500 1450
F 0 "C3" H 3525 1550 50  0000 L CNN
F 1 "25V 2200uf" H 3525 1350 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D13.0mm_P5.00mm" H 3500 1450 50  0001 C CNN
F 3 "" H 3500 1450 50  0001 C CNN
	1    3500 1450
	1    0    0    -1  
$EndComp
$Comp
L CP1 C1
U 1 1 5ADF3211
P 1950 3200
F 0 "C1" H 1975 3300 50  0000 L CNN
F 1 "5.5V 4F" H 1975 3100 50  0000 L CNN
F 2 "MyFootprints:Capacitors_THT_C_Disc_D27.0mm_W6.0mm_P6mm" H 1950 3200 50  0001 C CNN
F 3 "" H 1950 3200 50  0001 C CNN
	1    1950 3200
	1    0    0    -1  
$EndComp
$Comp
L CP1 C4
U 1 1 5ADF34CE
P 5200 2500
F 0 "C4" H 5225 2600 50  0000 L CNN
F 1 "1uf" H 5225 2400 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D4.0mm_P2.00mm" H 5200 2500 50  0001 C CNN
F 3 "" H 5200 2500 50  0001 C CNN
	1    5200 2500
	1    0    0    -1  
$EndComp
$EndSCHEMATC
